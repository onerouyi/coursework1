
from netmiko import ConnectHandler

iosv_R1 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.122.40',
    'username': 'franklin',
    'password': 'cisco',
}

iosv_R2 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.122.41',
    'username': 'franklin',
    'password': 'cisco',
}

iosv_R3 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.122.42',
    'username': 'franklin',
    'password': 'cisco',
}

# Loopback interace commands

with open('iosv_loopback') as f:
    lines = f.read().splitlines()
print (lines)


all_devices = [iosv_R1, iosv_R3]

for devices in all_devices:
    net_connect = ConnectHandler(**devices)
    output = net_connect.send_config_set(lines)
    output = net_connect.send_command('show ip int brief') # Retrieve and displays the interFace ConFig
    print (output) 

# OSPF process command

with open('iosv_ospf') as f:
    lines = f.read().splitlines()
print (lines)

all_devices = [iosv_R2]

for devices in all_devices:
    net_connect = ConnectHandler(**devices)
    output = net_connect.send_config_set(lines)
    output = net_connect.send_command('show ip protocol') # Retrieve and displays and OSPF process
    print (output) 